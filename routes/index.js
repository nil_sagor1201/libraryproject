const express = require('express')
const router = express.Router()



const genre = require('./genre')
const book = require('./book')
const author = require('./author')
const bookinstance = require('./bookinstance')

router.use('genre', genre)
router.use('author', author)
router.use('book', book)
router.use('bookinstance', bookinstance)


// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' })
// })

module.exports = router
