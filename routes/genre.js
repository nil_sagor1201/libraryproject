const express = require('express')
const router = express.Router()

// create
router.get('/genre/create', genre_create_get)
router.post('/genre/create', genre_create_get)

//delete
router.get('/genre/:id/delete', genre_create_get)
router.post('/genre/:id/delete', genre_create_get)


//update
router.get('/genre/:id/update', genre_create_get)
router.post('/genre/:id/update', genre_create_get)

// for one genre
router.get('/genre/:id', genre_create_get)

// for list all genre 
router.get('/genre', genre_create_get)

module.exports = router