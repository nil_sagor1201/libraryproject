const express = require('express')
const router = express.Router()

// create
router.get('/bookInstance/create', bookInstance_create_get)
router.post('/bookInstance/create', bookInstance_create_get)

//delete
router.get('/bookInstance/:id/delete', bookInstance_create_get)
router.post('/bookInstance/:id/delete', bookInstance_create_get)


//update
router.get('/bookInstance/:id/update', bookInstance_create_get)
router.post('/bookInstance/:id/update', bookInstance_create_get)

// for one bookInstance
router.get('/bookInstance/:id', bookInstance_create_get)

// for list all bookInstance 
router.get('/bookInstance', bookInstance_create_get)

module.exports = router