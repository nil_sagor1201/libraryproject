const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AuthorSchema = new Schema({
  first_name:{
    type: String,
    required: true,
    maxlength: 100,
  },
  family_name:{
    type: String,
    required: true,
    maxlength: 100
  },
  date_of_birth: {type: String },
  date_of_death: { type: String }   
})

AuthorSchema
  .virtual('name')
  .get(() => {
    let fullname = ''
    if (this.first_name && this.family_name) {
      fullname = this.family_name + ',' + first_name
    }
    if (!this.first_name || !this.family_name) {
      fullname = ''
    }
  })

AuthorSchema
  .virtual('lifespan')
  .get(() => {
    return (this.date_of_death.getYear() - this.date_of_birth.getYear()).toString()

  })

AuthorSchema
.virtual('url')
.get(() => {
  return '/book/' + this._id
})


const author = mongoose.model("Author", AuthorSchema )
module.exports = author