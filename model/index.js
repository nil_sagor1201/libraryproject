const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/libraryProject', 
  { useNewUrlParser: true, 
    useUnifiedTopology: true })


const Author = require('./Author')
const Book = require('./Book')
const BookInstance = require('./BookInstance')
const Genre = require('./Genre')

// const Cat = mongoose.model('Cat', { name: String })

// const kitty = new Cat({ name: 'Zildjian' })
// kitty.save().then(() => console.log('meow'))

module.exports = {
  Author,
  Book,
  BookInstance,
  Genre
}