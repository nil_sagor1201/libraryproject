const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BookInstanceSchema = new Schema({
  
  book:{
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Book'
  },
  imprint: {type: String, required: true },
  status: {
    type: String, 
    required: true,
    enum: ['Available', 'Maintenance', 'Loaned', "Reserved"]
  },
  due_date: {
    type: Date,
    default: Date.now
  }     
})





BookInstanceSchema
.virtual('url')
.get(() => {
  return '/book/' + this._id
})


const bookInstance = mongoose.model("BookInstance", BookInstanceSchema )
module.exports = bookInstance