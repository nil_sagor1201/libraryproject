const authorController = require('./authorController')
const genreController = require('./genreController')
const bookController = require('./bookController')
const bookInstanceController = require('./bookInstanceController')


module.exports = {
  authorController,
  genreController,
  bookController,
  bookInstanceController
}